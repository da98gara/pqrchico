<?php
declare(strict_types=1);
namespace Vokuro\Forms;

use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Form;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\PresenceOf;

class LoginForm extends Form
{
    public function initialize()
    {
        // Email
        $email = new Text('email', [
            'placeholder' => 'Email',
        ]);

        $email->addValidators([
            new PresenceOf([
                'message' => 'El Correo es necesario',
            ]),
            new Email([
                'message' => 'El Correo no es Correcto',
            ]),
        ]);

        $this->add($email);

        // Password
        $password = new Password('password', [
            'placeholder' => 'Password',
        ]);
        $password->addValidator(new PresenceOf([
            'message' => 'Se requiere la contraseña',
        ]));
        $password->clear();

        $this->add($password);

        // Remember
        $remember = new Check('remember', [
            'value' => 'yes',
            'id'    => 'login-remember',
        ]);
        $remember->setLabel('Recordar mis datos');

        $this->add($remember);

        // CSRF
        $csrf = new Hidden('csrf');
        $csrf->addValidator(new Identical([
            'value'   => $this->security->getRequestToken(),
            'message' => 'CSRF validation failed',
        ]));
        $csrf->clear();

        $this->add($csrf);

        $this->add(new Submit('Login', [
            'value' => 'Iniciar Sesion',
            'class' => 'btn btn-primary block full-width m-b'
        ]));
    }
}
