<?php

declare(strict_types=1);

namespace Vokuro\Forms;

use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Form;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Vokuro\Models\Profiles;
use Phalcon\Forms\Element\Date;

class PqrForm extends Form {
/**
     * @param null $entity
     * @param array $options
     */
    public function initialize(){
         // In edition the id is hidden

         //identificacion

         $identificacion = new Text('identificacion', [
            'placeholder' => 'cedula',
        ]);

        $this->add($identificacion);

         // complaintText
         $complaintText = new TextArea('complaintText', [
            'placeholder' => 'ingresa un texto',
        ]);
        $this->add($complaintText);

        // complaintDate 

        $complaintDate = new Date('complaintDate', [
            'Placeholder' => 'ingrese la fecha ',
        ]);
        $this->add($complaintDate);

        //senderName

        $senderName  = new Text('senderName', [
            'Placeholder' => 'ingrese el remitente',
        ]);
        $this->add($senderName);

        //senderAddress

        $senderAddress = new Text('senderAddress', [
            'Placeholder' => 'ingrese la  direccion del remitente',
        ]);
        $this->add($senderAddress);

        //recipientName

        $recipientName = new Text('recipientName', [
            'Placeholder' => 'ingrece el nombre del remitente',
        ]);

        $this->add($recipientName);

        //recipientAddress 

        $recipientAddress = new Text('recipientAddress', [
            'Placeholder' => 'ingrese la direccion del remitente',
        ]);
        $this->add($recipientAddress);

        //serviceDate
        $serviceDate = new Date('serviceDate', [
            'placeholder' => 'ingrese la fecha del servicio ',
        ]);
        $this->add($serviceDate);

        //documentImage

        $documentImage = new Text('documentImage', [
            'placeholder' => 'ingrese un documento por el momento  texto',
        ]);
        $this->add($documentImage);

        // guideImage

        $guideImage = new Text('guideImage', [
            'placeholder' => 'ingrese la guia  de la imagne por el momento  texto',

        ]);
        $this->add($guideImage);

        //admissionImage

        $admissionImage = new Text('admissionImage', [

            'placeholder' => 'ingrese la imagen de la admision',
        ]);
        $this->add($admissionImage);

        //deliverImage
        $deliverImage = new Text('deliverImage', [
            'placeholder' => 'ingrese  la imagen en el momento texto',
        ]);
        $this->add($deliverImage);

        //complaintType 
        $complaintType = new Text('complaintType', [
            'placeholder' => 'tipo de queja ',

        ]);

        $this->add($complaintType);

        //dueDate
        $dueDate = new Date('dueDate', [
            'palceholder' => 'fecha de vencimiento',
        ]);

        $this->add($dueDate);

        //createdAt
        $createdAt = new  Date('createdAt', [
            'placeholder' => 'create  dat',
        ]);
              $this-> add ($createdAt) ;
        //updatedAt
        
        $updatedAt = new Date ('updatedAt',[
            'palceholder' => 'create dat',
        ]); 

        $this-> add ($updatedAt );
       
    }

}