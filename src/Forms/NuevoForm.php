<?php

declare(strict_types=1);

namespace Vokuro\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Text;


class NuevoForm extends Form
{
    /**
     * @param null $entity
     * @param array $options
     */

    public function initialize()
    {
        // In edition the id is hidden

        $hola = new Text('hola', [
            'placeholder' => 'cedula'
        ]);

        $this->add($hola);
    } //fin metodo
}// fin class