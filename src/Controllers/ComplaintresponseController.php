<?php
declare(strict_types=1);

namespace complaintresponse;


use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model;
use complaintresponse\Complaintresponse;

class ComplaintresponseController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        //
    }

    /**
     * Searches for complaintresponse
     */
    public function searchAction()
    {
        $numberPage = $this->request->getQuery('page', 'int', 1);
        $parameters = Criteria::fromInput($this->di, '\complaintresponse\Complaintresponse', $_GET)->getParams();
        $parameters['order'] = "id";

        $paginator   = new Model(
            [
                'model'      => '\complaintresponse\Complaintresponse',
                'parameters' => $parameters,
                'limit'      => 10,
                'page'       => $numberPage,
            ]
        );

        $paginate = $paginator->paginate();

        if (0 === $paginate->getTotalItems()) {
            $this->flash->notice("The search did not find any complaintresponse");

            $this->dispatcher->forward([
                "controller" => "complaintresponse",
                "action" => "index"
            ]);

            return;
        }

        $this->view->page = $paginate;
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {
        //
    }

    /**
     * Edits a complaintresponse
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {
            $complaintresponse = Complaintresponse::findFirstByid($id);
            if (!$complaintresponse) {
                $this->flash->error("complaintresponse was not found");

                $this->dispatcher->forward([
                    'controller' => "complaintresponse",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $complaintresponse->id;

            $this->tag->setDefault("id", $complaintresponse->id);
            $this->tag->setDefault("complaint_id", $complaintresponse->complaint_id);
            $this->tag->setDefault("responseDate", $complaintresponse->responseDate);
            $this->tag->setDefault("response", $complaintresponse->response);
            $this->tag->setDefault("responseUser", $complaintresponse->responseUser);
            $this->tag->setDefault("created_at", $complaintresponse->created_at);
            $this->tag->setDefault("updated_at", $complaintresponse->updated_at);
            
        }
    }

    /**
     * Creates a new complaintresponse
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "complaintresponse",
                'action' => 'index'
            ]);

            return;
        }

        $complaintresponse = new Complaintresponse();
        $complaintresponse->complaintId = $this->request->getPost("complaint_id", "int");
        $complaintresponse->responseDate = $this->request->getPost("responseDate", "int");
        $complaintresponse->response = $this->request->getPost("response", "int");
        $complaintresponse->responseUser = $this->request->getPost("responseUser", "int");
        $complaintresponse->createdAt = $this->request->getPost("created_at", "int");
        $complaintresponse->updatedAt = $this->request->getPost("updated_at", "int");
        

        if (!$complaintresponse->save()) {
            foreach ($complaintresponse->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "complaintresponse",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("complaintresponse was created successfully");

        $this->dispatcher->forward([
            'controller' => "complaintresponse",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a complaintresponse edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "complaintresponse",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $complaintresponse = Complaintresponse::findFirstByid($id);

        if (!$complaintresponse) {
            $this->flash->error("complaintresponse does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "complaintresponse",
                'action' => 'index'
            ]);

            return;
        }

        $complaintresponse->complaintId = $this->request->getPost("complaint_id", "int");
        $complaintresponse->responseDate = $this->request->getPost("responseDate", "int");
        $complaintresponse->response = $this->request->getPost("response", "int");
        $complaintresponse->responseUser = $this->request->getPost("responseUser", "int");
        $complaintresponse->createdAt = $this->request->getPost("created_at", "int");
        $complaintresponse->updatedAt = $this->request->getPost("updated_at", "int");
        

        if (!$complaintresponse->save()) {

            foreach ($complaintresponse->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "complaintresponse",
                'action' => 'edit',
                'params' => [$complaintresponse->id]
            ]);

            return;
        }

        $this->flash->success("complaintresponse was updated successfully");

        $this->dispatcher->forward([
            'controller' => "complaintresponse",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a complaintresponse
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $complaintresponse = Complaintresponse::findFirstByid($id);
        if (!$complaintresponse) {
            $this->flash->error("complaintresponse was not found");

            $this->dispatcher->forward([
                'controller' => "complaintresponse",
                'action' => 'index'
            ]);

            return;
        }

        if (!$complaintresponse->delete()) {

            foreach ($complaintresponse->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "complaintresponse",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("complaintresponse was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "complaintresponse",
            'action' => "index"
        ]);
    }
}
