<?php
declare(strict_types=1);

 

use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model;


class ComplainttypesController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        //
    }

    /**
     * Searches for complainttypes
     */
    public function searchAction()
    {
        $numberPage = $this->request->getQuery('page', 'int', 1);
        $parameters = Criteria::fromInput($this->di, 'Complainttypes', $_GET)->getParams();
        $parameters['order'] = "id";

        $paginator   = new Model(
            [
                'model'      => 'Complainttypes',
                'parameters' => $parameters,
                'limit'      => 10,
                'page'       => $numberPage,
            ]
        );

        $paginate = $paginator->paginate();

        if (0 === $paginate->getTotalItems()) {
            $this->flash->notice("The search did not find any complainttypes");

            $this->dispatcher->forward([
                "controller" => "complainttypes",
                "action" => "index"
            ]);

            return;
        }

        $this->view->page = $paginate;
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {
        //
    }

    /**
     * Edits a complainttype
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {
            $complainttype = Complainttypes::findFirstByid($id);
            if (!$complainttype) {
                $this->flash->error("complainttype was not found");

                $this->dispatcher->forward([
                    'controller' => "complainttypes",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $complainttype->getId();

            $this->tag->setDefault("id", $complainttype->getId());
            $this->tag->setDefault("name", $complainttype->getName());
            $this->tag->setDefault("created_at", $complainttype->getCreatedAt());
            $this->tag->setDefault("updated_at", $complainttype->getUpdatedAt());
            
        }
    }

    /**
     * Creates a new complainttype
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "complainttypes",
                'action' => 'index'
            ]);

            return;
        }

        $complainttype = new Complainttypes();
        $complainttype->setname($this->request->getPost("name", "int"));
        $complainttype->setcreatedAt($this->request->getPost("created_at", "int"));
        $complainttype->setupdatedAt($this->request->getPost("updated_at", "int"));
        

        if (!$complainttype->save()) {
            foreach ($complainttype->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "complainttypes",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("complainttype was created successfully");

        $this->dispatcher->forward([
            'controller' => "complainttypes",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a complainttype edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "complainttypes",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $complainttype = Complainttypes::findFirstByid($id);

        if (!$complainttype) {
            $this->flash->error("complainttype does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "complainttypes",
                'action' => 'index'
            ]);

            return;
        }

        $complainttype->setname($this->request->getPost("name", "int"));
        $complainttype->setcreatedAt($this->request->getPost("created_at", "int"));
        $complainttype->setupdatedAt($this->request->getPost("updated_at", "int"));
        

        if (!$complainttype->save()) {

            foreach ($complainttype->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "complainttypes",
                'action' => 'edit',
                'params' => [$complainttype->getId()]
            ]);

            return;
        }

        $this->flash->success("complainttype was updated successfully");

        $this->dispatcher->forward([
            'controller' => "complainttypes",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a complainttype
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $complainttype = Complainttypes::findFirstByid($id);
        if (!$complainttype) {
            $this->flash->error("complainttype was not found");

            $this->dispatcher->forward([
                'controller' => "complainttypes",
                'action' => 'index'
            ]);

            return;
        }

        if (!$complainttype->delete()) {

            foreach ($complainttype->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "complainttypes",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("complainttype was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "complainttypes",
            'action' => "index"
        ]);
    }
}
