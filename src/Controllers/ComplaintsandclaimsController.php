<?php

declare(strict_types=1);

namespace Vokuro\Controllers;


use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model;
use vokuro\Forms\ComplaintsandclaimsForm;
use Vokuro\Models\Complaintsandclaims;
use Vokuro\Models\Complaintparameters;

class ComplaintsandclaimsController extends ControllerBase
{
    /**
     * Index action
     */
    public function initialize(): void
    {
        
    }

    public function indexAction()
    {
       // $this->view->setVar('form', new ComplaintsandclaimsForm());
    return  $this->view->pick("creacionPQR/crearPQR");
    }

    /**
     * Searches for complaintsandclaims
     */
    public function searchAction()
    {
        $numberPage = $this->request->getQuery('page', 'int', 1);
        $parameters = Criteria::fromInput($this->di, '\complaintsandclaims\Complaintsandclaims', $_GET)->getParams();
        $parameters['order'] = "id";

        $paginator   = new Model(
            [
                'model'      => '\complaintsandclaims\Complaintsandclaims',
                'parameters' => $parameters,
                'limit'      => 10,
                'page'       => $numberPage,
            ]
        );

        $paginate = $paginator->paginate();

        if (0 === $paginate->getTotalItems()) {
            $this->flash->notice("The search did not find any complaintsandclaims");

            $this->dispatcher->forward([
                "controller" => "complaintsandclaims",
                "action" => "index"
            ]);

            return;
        }

        $this->view->page = $paginate;
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {
        //
    }

    /**
     * Edits a complaintsandclaim
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {
            $complaintsandclaim = Complaintsandclaims::findFirstByid($id);
            if (!$complaintsandclaim) {
                $this->flash->error("complaintsandclaim was not found");

                $this->dispatcher->forward([
                    'controller' => "complaintsandclaims",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $complaintsandclaim->id;

            $this->tag->setDefault("id", $complaintsandclaim->id);
            $this->tag->setDefault("cun", $complaintsandclaim->cun);
            $this->tag->setDefault("identification", $complaintsandclaim->identification);
            $this->tag->setDefault("complaintText", $complaintsandclaim->complaintText);
            $this->tag->setDefault("complaintDate", $complaintsandclaim->complaintDate);
            $this->tag->setDefault("senderName", $complaintsandclaim->senderName);
            $this->tag->setDefault("senderAddress", $complaintsandclaim->senderAddress);
            $this->tag->setDefault("recipientName", $complaintsandclaim->recipientName);
            $this->tag->setDefault("recipientAddress", $complaintsandclaim->recipientAddress);
            $this->tag->setDefault("serviceDate", $complaintsandclaim->serviceDate);
            $this->tag->setDefault("documentImage", $complaintsandclaim->documentImage);
            $this->tag->setDefault("guideImage", $complaintsandclaim->guideImage);
            $this->tag->setDefault("admissionImage", $complaintsandclaim->admissionImage);
            $this->tag->setDefault("deliverImage", $complaintsandclaim->deliverImage);
            $this->tag->setDefault("complaintType", $complaintsandclaim->complaintType);
            $this->tag->setDefault("dueDate", $complaintsandclaim->dueDate);
            $this->tag->setDefault("created_at", $complaintsandclaim->created_at);
            $this->tag->setDefault("updated_at", $complaintsandclaim->updated_at);
        }
    }

    /**
     * Creates a new complaintsandclaim
     */


    public function createAction()
    {
        $form = new ComplaintsandclaimsForm();
        
        if ($this->request->isPost()) {
            if (!$form->isValid($this->request->getPost())) {

                foreach ($form->getMessages() as $message) {
                    $this->flash->error((string) $message);
                }
            } else {
                $parameters1 = new Complaintparameters();
                $cun1 = $parameters1::createCUN();
                $Complaintsandclaims  = new Complaintsandclaims([
                    'cun' => $cun1,
                    'identification' => $this->request->getPost("identification"),
                    'complaintText' => $this->request->getPost("complaintText"),
                    'complaintDate' => $this->request->getPost("complaintDate"),
                    'senderName' => $this->request->getPost("senderName"),
                    'senderAddress' => $this->request->getPost("senderAddress"),
                    'recipientName' => $this->request->getPost("recipientName"),
                    'recipientAddress'  => $this->request->getPost("recipientAddress"),
                    'serviceDate' => $this->request->getPost("serviceDate"),
                    'documentImage' => $this->request->getPost("documentImage"),
                    'guideImage' => $this->request->getPost("guideImage"),
                    'admissionImage' => $this->request->getPost("admissionImage"),
                    'deliverImage' => $this->request->getPost("deliverImage"),
                    'complaintType' => $this->request->getPost("complaintType"),
                    'dueDate' => $this->request->getPost("dueDate"),
                    'createdAt' => $this->request->getPost("created_at"),
                    'updatedAt' => $this->request->getPost("updated_at"),
                ]);
                if (!$Complaintsandclaims->save()) {
                    foreach ($Complaintsandclaims->getMessages() as $message) {
                        $this->flash->error((string) $message);
                    }
                } else {
                    $this->flash->success("la PQR fue creada exitosamente");
                }
            }
        }
        
        $this->view->setVar('form', $form);
        
        return  $this->view->pick("vokuro/CreacionPQR/CrearPQR");
    } // Fin  metodo Crear



    /**
     * Saves a complaintsandclaim edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "complaintsandclaims",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $complaintsandclaim = Complaintsandclaims::findFirstByid($id);

        if (!$complaintsandclaim) {
            $this->flash->error("complaintsandclaim does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "complaintsandclaims",
                'action' => 'index'
            ]);

            return;
        }

        $complaintsandclaim->cun = $this->request->getPost("cun", "int");
        $complaintsandclaim->identification = $this->request->getPost("identification", "int");
        $complaintsandclaim->complaintText = $this->request->getPost("complaintText", "int");
        $complaintsandclaim->complaintDate = $this->request->getPost("complaintDate", "int");
        $complaintsandclaim->senderName = $this->request->getPost("senderName", "int");
        $complaintsandclaim->senderAddress = $this->request->getPost("senderAddress", "int");
        $complaintsandclaim->recipientName = $this->request->getPost("recipientName", "int");
        $complaintsandclaim->recipientAddress = $this->request->getPost("recipientAddress", "int");
        $complaintsandclaim->serviceDate = $this->request->getPost("serviceDate", "int");
        $complaintsandclaim->documentImage = $this->request->getPost("documentImage", "int");
        $complaintsandclaim->guideImage = $this->request->getPost("guideImage", "int");
        $complaintsandclaim->admissionImage = $this->request->getPost("admissionImage", "int");
        $complaintsandclaim->deliverImage = $this->request->getPost("deliverImage", "int");
        $complaintsandclaim->complaintType = $this->request->getPost("complaintType", "int");
        $complaintsandclaim->dueDate = $this->request->getPost("dueDate", "int");
        $complaintsandclaim->createdAt = $this->request->getPost("created_at", "int");
        $complaintsandclaim->updatedAt = $this->request->getPost("updated_at", "int");


        if (!$complaintsandclaim->save()) {

            foreach ($complaintsandclaim->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "complaintsandclaims",
                'action' => 'edit',
                'params' => [$complaintsandclaim->id]
            ]);

            return;
        }

        $this->flash->success("complaintsandclaim was updated successfully");

        $this->dispatcher->forward([
            'controller' => "complaintsandclaims",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a complaintsandclaim
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $complaintsandclaim = Complaintsandclaims::findFirstByid($id);
        if (!$complaintsandclaim) {
            $this->flash->error("complaintsandclaim was not found");

            $this->dispatcher->forward([
                'controller' => "complaintsandclaims",
                'action' => 'index'
            ]);

            return;
        }

        if (!$complaintsandclaim->delete()) {

            foreach ($complaintsandclaim->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "complaintsandclaims",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("complaintsandclaim was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "complaintsandclaims",
            'action' => "index"
        ]);
    }
}
