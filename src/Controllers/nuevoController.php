<?php
declare(strict_types=1);

namespace Vokuro\Controllers;

use Phalcon\Mvc\Model\Criteria;
use Vokuro\Forms\NuevoForm;
use Phalcon\Paginator\Adapter\Model;
use Phalcon\Paginator\Adapter\QueryBuilder as Paginator;

class nuevoController  extends ControllerBase
{

    public function indexAction () {

        $this->view->setVar('form', new NuevoForm());
        return  $this->view->pick("creacionPQR/Nuevo");

    }// fin 
}



