<?php

declare(strict_types=1);

namespace Vokuro\Controllers;


use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model;
use vokuro\Forms\prueba2Forms;
use Vokuro\Models\Complaintsandclaims;
use Vokuro\Models\Complaintparameters;




class Prueba2Controller extends ControllerBase
{
    public function initialize(): void
    {
        $this->view->setTemplateBefore('private');
    }

    public function indexAction()
    {
        $this->view->setVar('form', new prueba2Forms());
        return  $this->view->pick("creacionPQR/create");
    } // fin 
}
