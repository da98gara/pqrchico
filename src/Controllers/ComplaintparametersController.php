<?php
declare(strict_types=1);

 

use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model;


class ComplaintparametersController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        //
    }

    /**
     * Searches for complaintparameters
     */
    public function searchAction()
    {
        $numberPage = $this->request->getQuery('page', 'int', 1);
        $parameters = Criteria::fromInput($this->di, 'Complaintparameters', $_GET)->getParams();
        $parameters['order'] = "id";

        $paginator   = new Model(
            [
                'model'      => 'Complaintparameters',
                'parameters' => $parameters,
                'limit'      => 10,
                'page'       => $numberPage,
            ]
        );

        $paginate = $paginator->paginate();

        if (0 === $paginate->getTotalItems()) {
            $this->flash->notice("The search did not find any complaintparameters");

            $this->dispatcher->forward([
                "controller" => "complaintparameters",
                "action" => "index"
            ]);

            return;
        }

        $this->view->page = $paginate;
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {
        //
    }

    /**
     * Edits a complaintparameter
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {
            $complaintparameter = Complaintparameters::findFirstByid($id);
            if (!$complaintparameter) {
                $this->flash->error("complaintparameter was not found");

                $this->dispatcher->forward([
                    'controller' => "complaintparameters",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $complaintparameter->getId();

            $this->tag->setDefault("id", $complaintparameter->getId());
            $this->tag->setDefault("pqrEmail", $complaintparameter->getPqremail());
            $this->tag->setDefault("responseModel", $complaintparameter->getResponsemodel());
            $this->tag->setDefault("daysDue", $complaintparameter->getDaysdue());
            $this->tag->setDefault("nextNumber", $complaintparameter->getNextnumber());
            $this->tag->setDefault("operatorId", $complaintparameter->getOperatorid());
            $this->tag->setDefault("created_at", $complaintparameter->getCreatedAt());
            $this->tag->setDefault("updated_at", $complaintparameter->getUpdatedAt());
            
        }
    }

    /**
     * Creates a new complaintparameter
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "complaintparameters",
                'action' => 'index'
            ]);

            return;
        }

        $complaintparameter = new Complaintparameters();
        $complaintparameter->setpqrEmail($this->request->getPost("pqrEmail", "int"));
        $complaintparameter->setresponseModel($this->request->getPost("responseModel", "int"));
        $complaintparameter->setdaysDue($this->request->getPost("daysDue", "int"));
        $complaintparameter->setnextNumber($this->request->getPost("nextNumber", "int"));
        $complaintparameter->setoperatorId($this->request->getPost("operatorId", "int"));
        $complaintparameter->setcreatedAt($this->request->getPost("created_at", "int"));
        $complaintparameter->setupdatedAt($this->request->getPost("updated_at", "int"));
        

        if (!$complaintparameter->save()) {
            foreach ($complaintparameter->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "complaintparameters",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("complaintparameter was created successfully");

        $this->dispatcher->forward([
            'controller' => "complaintparameters",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a complaintparameter edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "complaintparameters",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $complaintparameter = Complaintparameters::findFirstByid($id);

        if (!$complaintparameter) {
            $this->flash->error("complaintparameter does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "complaintparameters",
                'action' => 'index'
            ]);

            return;
        }

        $complaintparameter->setpqrEmail($this->request->getPost("pqrEmail", "int"));
        $complaintparameter->setresponseModel($this->request->getPost("responseModel", "int"));
        $complaintparameter->setdaysDue($this->request->getPost("daysDue", "int"));
        $complaintparameter->setnextNumber($this->request->getPost("nextNumber", "int"));
        $complaintparameter->setoperatorId($this->request->getPost("operatorId", "int"));
        $complaintparameter->setcreatedAt($this->request->getPost("created_at", "int"));
        $complaintparameter->setupdatedAt($this->request->getPost("updated_at", "int"));
        

        if (!$complaintparameter->save()) {

            foreach ($complaintparameter->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "complaintparameters",
                'action' => 'edit',
                'params' => [$complaintparameter->getId()]
            ]);

            return;
        }

        $this->flash->success("complaintparameter was updated successfully");

        $this->dispatcher->forward([
            'controller' => "complaintparameters",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a complaintparameter
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $complaintparameter = Complaintparameters::findFirstByid($id);
        if (!$complaintparameter) {
            $this->flash->error("complaintparameter was not found");

            $this->dispatcher->forward([
                'controller' => "complaintparameters",
                'action' => 'index'
            ]);

            return;
        }

        if (!$complaintparameter->delete()) {

            foreach ($complaintparameter->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "complaintparameters",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("complaintparameter was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "complaintparameters",
            'action' => "index"
        ]);
    }
}
