<?php

declare(strict_types=1);

namespace Vokuro\Controllers;


use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model;
use Vokuro\Forms\PqrForm;
use Vokuro\Models\Complaintsandclaims;
use Vokuro\Models\Complaintparameters;

class PqrController  extends ControllerBase {

    public function indexAction () {

        $this->view->setVar('form', new PqrForm());
        return  $this->view->pick("creacionPQR/new");

    }// fin 

    public function newAction()
    {
         

        $form = new PqrForm();
        
        if ($this->request->isPost()) {
            if (!$form->isValid($this->request->getPost())) {

                foreach ($form->getMessages() as $message) {
                    $this->flash->error((string) $message);
                }
            } else {
                $parameters1 = new Complaintparameters();
                $cun1 = $parameters1::createCUN();
                $Complaintsandclaims  = new Complaintsandclaims([
                    'cun' => $cun1 ,
                    'identification' => $this->request->getPost("identification"),
                    'complaintText' => $this->request->getPost("complaintText"),
                    'complaintDate' => $this->request->getPost("complaintDate"),
                    'senderName' => $this->request->getPost("senderName"),
                    'senderAddress' => $this->request->getPost("senderAddress"),
                    'recipientName' => $this->request->getPost("recipientName"),
                    'recipientAddress'  => $this->request->getPost("recipientAddress"),
                    'serviceDate' => $this->request->getPost("serviceDate"),
                    'documentImage' => $this->request->getPost("documentImage"),
                    'guideImage' => $this->request->getPost("guideImage"),
                    'admissionImage' => $this->request->getPost("admissionImage"),
                    'deliverImage' => $this->request->getPost("deliverImage"),
                    'complaintType' => $this->request->getPost("complaintType"),
                    'dueDate' => $this->request->getPost("dueDate"),
                    'createdAt' => $this->request->getPost("created_at"),
                    'updatedAt' => $this->request->getPost("updated_at"),
                ]);
                if (!$Complaintsandclaims->save()) {
                    foreach ($Complaintsandclaims->getMessages() as $message) {
                        $this->flash->error((string) $message);
                        
                    }
                } else {
                    $this->flash->success("la PQR fue creada exitosamente");
                    
                }
            }

            $this->view->setVar('form', $form);
            return  $this->view->pick("themes/session/login");
        }
       
        
    } // Fin  metodo Crear
}