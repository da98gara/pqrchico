<?php

namespace Vokuro\Models;

class Complaintsandclaims extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $cun;

    /**
     *
     * @var integer
     */
    public $identification;

    /**
     *
     * @var string
     */
    public $complaintText;

    /**
     *
     * @var string
     */
    public $complaintDate;

    /**
     *
     * @var string
     */
    public $senderName;

    /**
     *
     * @var string
     */
    public $senderAddress;

    /**
     *
     * @var string
     */
    public $recipientName;

    /**
     *
     * @var string
     */
    public $recipientAddress;

    /**
     *
     * @var string
     */
    public $serviceDate;

    /**
     *
     * @var string
     */
    public $documentImage;

    /**
     *
     * @var string
     */
    public $guideImage;

    /**
     *
     * @var string
     */
    public $admissionImage;

    /**
     *
     * @var string
     */
    public $deliverImage;

    /**
     *
     * @var string
     */
    public $complaintType;

    /**
     *
     * @var string
     */
    public $dueDate;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("loginback");
        $this->setSource("complaintsandclaims");
        $this->hasMany('id', 'complaintsandclaims\Complaintresponse', 'complaint_id', ['alias' => 'Complaintresponse']);
        $this->belongsTo('identification', 'complaintsandclaims\Customers', 'id', ['alias' => 'Customers']);
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Complaintsandclaims[]|Complaintsandclaims|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null): \Phalcon\Mvc\Model\ResultsetInterface
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Complaintsandclaims|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
