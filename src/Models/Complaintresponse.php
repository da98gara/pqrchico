<?php

namespace complaintresponse;

class Complaintresponse extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $complaint_id;

    /**
     *
     * @var string
     */
    public $responseDate;

    /**
     *
     * @var string
     */
    public $response;

    /**
     *
     * @var string
     */
    public $responseUser;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("loginback");
        $this->setSource("complaintresponse");
        $this->belongsTo('complaint_id', 'complaintresponse\Complaintsandclaims', 'id', ['alias' => 'Complaintsandclaims']);
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Complaintresponse[]|Complaintresponse|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null): \Phalcon\Mvc\Model\ResultsetInterface
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Complaintresponse|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
