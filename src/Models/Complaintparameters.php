<?php

namespace  Vokuro\Models;

class Complaintparameters extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $pqrEmail;

    /**
     *
     * @var string
     */
    protected $responseModel;

    /**
     *
     * @var integer
     */
    protected $daysDue;

    /**
     *
     * @var integer
     */
    protected $nextNumber;

    /**
     *
     * @var string
     */
    protected $operatorId;

    /**
     *
     * @var string
     */
    protected $created_at;

    /**
     *
     * @var string
     */
    protected $updated_at;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field pqrEmail
     *
     * @param string $pqrEmail
     * @return $this
     */
    public function setPqrEmail($pqrEmail)
    {
        $this->pqrEmail = $pqrEmail;

        return $this;
    }

    /**
     * Method to set the value of field responseModel
     *
     * @param string $responseModel
     * @return $this
     */
    public function setResponseModel($responseModel)
    {
        $this->responseModel = $responseModel;

        return $this;
    }

    /**
     * Method to set the value of field daysDue
     *
     * @param integer $daysDue
     * @return $this
     */
    public function setDaysDue($daysDue)
    {
        $this->daysDue = $daysDue;

        return $this;
    }

    /**
     * Method to set the value of field nextNumber
     *
     * @param integer $nextNumber
     * @return $this
     */
    public function setNextNumber($nextNumber)
    {
        $this->nextNumber = $nextNumber;

        return $this;
    }

    /**
     * Method to set the value of field operatorId
     *
     * @param string $operatorId
     * @return $this
     */
    public function setOperatorId($operatorId)
    {
        $this->operatorId = $operatorId;

        return $this;
    }

    /**
     * Method to set the value of field created_at
     *
     * @param string $created_at
     * @return $this
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Method to set the value of field updated_at
     *
     * @param string $updated_at
     * @return $this
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field pqrEmail
     *
     * @return string
     */
    public function getPqrEmail()
    {
        return $this->pqrEmail;
    }

    /**
     * Returns the value of field responseModel
     *
     * @return string
     */
    public function getResponseModel()
    {
        return $this->responseModel;
    }

    /**
     * Returns the value of field daysDue
     *
     * @return integer
     */
    public function getDaysDue()
    {
        return $this->daysDue;
    }

    /**
     * Returns the value of field nextNumber
     *
     * @return integer
     */
    public function getNextNumber()
    {
        return $this->nextNumber;
    }

    /**
     * Returns the value of field operatorId
     *
     * @return string
     */
    public function getOperatorId()
    {
        return $this->operatorId;
    }

    /**
     * Returns the value of field created_at
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Returns the value of field updated_at
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("loginback");
        $this->setSource("complaintparameters");
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Complaintparameters[]|Complaintparameters|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null): \Phalcon\Mvc\Model\ResultsetInterface
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Complaintparameters|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }




    public static function  createCUN()
    {

        $parameter = new Complaintparameters();

        $parameters = Complaintparameters::findFirst('id = 1');
        $operator =  $parameters->operatorId;
        $NexNum = str_pad($parameters->NextNumber, 10, "0", STR_PAD_LEFT);
        $actualYear = date("y");
        $cun = $operator . $actualYear . $NexNum;
         
        $parameters->assign(
            [
                'nextNumber'=> $NexNum+1,
            ]

        );

        $parameters->save();

        return $cun;
        
    } // fin 
    
}
