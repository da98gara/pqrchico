<?php
declare(strict_types=1);

/**
 * This file is part of the Vökuró.
 *
 * (c) Phalcon Team <team@phalcon.io>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Vokuro\Providers;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Assets\Manager;

class AssetsProvider implements ServiceProviderInterface
{
    protected const VERSION = "1.0.0";
    /**
     * @var string
     */
    protected $providerName = 'assets';

    /**
     * @param DiInterface $di
     *
     * @return void
     */
    public function register(DiInterface $di): void
    {
        $assetManager = new Manager();

        $di->setShared($this->providerName, function () use ($assetManager) {
            $assetManager->collection('css')
                ->addCss('/css/bootstrap.min.css?dc=' . self::VERSION, true, true, [])
                ->addCss('/css/plugins/toastr/toastr.min.css?dc=' . self::VERSION, true, true, [])
                // ->addCss('/css/plugins/jquery.gritter.css?dc=' . self::VERSION, true, true, [])
                ->addCss('/css/animate.css?dc=' . self::VERSION, true, true, [])
                ->addCss('/css/style.css?dc=' . self::VERSION, true, true, [])
                ->addCss('/css/plugins/footable/footable.core.css?dc=' . self::VERSION, true, true, [])
                ->addCss('/css/icofont/icofont.min.css?dc=' . self::VERSION, true, true, []);
                /*
                * Steps Wizard
                */ 
                


            $assetManager->collection('js')
                // ->addJs('/js/core/jquery-3.1.1.min.js',  true, true, [])
                ->addJs('/js/jquery-1.9.1.min.js',  true, true, [])
                ->addJs('/js/core/popper.min.js?dc=' . self::VERSION, true, true, [])
                ->addJs('/js/core/bootstrap.js?dc=' . self::VERSION, true, true, [])
                ->addJs('/js/plugins/jquery.metisMenu.js?dc=' . self::VERSION, true, true, [])
                ->addJs('/js/plugins/jquery.slimscroll.min.js?dc=' . self::VERSION, true, true, [])
                ->addJs('/js/plugins/flot/jquery.flot.js?dc=' . self::VERSION, true, true, [])
                ->addJs('/js/plugins/flot/jquery.flot.tooltip.min.js?dc=' . self::VERSION, true, true, [])
                ->addJs('/js/plugins/flot/jquery.flot.spline.js?dc=' . self::VERSION, true, true, [])
                ->addJs('/js/plugins/flot/jquery.flot.resize.js?dc=' . self::VERSION, true, true, [])
                ->addJs('/js/plugins/flot/jquery.flot.pie.js?dc=' . self::VERSION, true, true, [])
                ->addJs('/js/plugins/peity/jquery.peity.min.js?dc=' . self::VERSION, true, true, [])
                ->addJs('/js/plugins/peity/peity-demo.js?dc=' . self::VERSION, true, true, [])
                ->addJs('/js/plugins/footable/footable.all.min.js?dc=' . self::VERSION, true, true, [])
                ->addJs('/js/inspinia.js?dc=' . self::VERSION, true, true, [])
                ->addJs('/js/plugins/pace/pace.min.js?dc=' . self::VERSION, true, true, [])
                ->addJs('/js/plugins/jquery-ui/jquery-ui.min.js?dc=' . self::VERSION, true, true, [])
                ->addJs('/js/plugins/gritter/jquery.gritter.min.js?dc=' . self::VERSION, true, true, [])
                ->addJs('/js/plugins/sparkline/jquery.sparkline.min.js?dc=' . self::VERSION, true, true, [])
                ->addJs('/js/plugins/chartjs/chart.min.js?dc=' . self::VERSION, true, true, [])
                ->addJs('/js/plugins/toastr/toastr.min.js');
            return $assetManager;
        });
    }
}
