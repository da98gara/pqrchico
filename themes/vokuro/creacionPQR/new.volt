<div>
    {{ flash.output() }}
</div>

<form method="post" action="{{ url("Pqr/new") }}">
    <br>
    <div>
        <div class="col-sm-10">
            {{ form.render('identificacion') }}
        </div>
    </div>
    <br>
    <div>
        <div class="col-sm-10">
            {{ form.render('complaintText') }}
        </div>
    </div>
    <br>
    <div>
        <div class="col-sm-10">
            {{ form.render('complaintDate') }}
        </div>
    </div>
    <br>
    <div>
        <div class="col-sm-10">
            {{ form.render('senderName') }}
        </div>
    </div>
    <br>
    <div>
        <div class="col-sm-10">
            {{ form.render('senderAddress') }}
        </div>
    </div>
    <br>
    <div>
        <div class="col-sm-10">
            {{ form.render('recipientName') }}
        </div>
    </div>
    <br>
    <div>
        <div class="col-sm-10">
            {{ form.render('recipientAddress') }}
        </div>
    </div>
    <br>
    <div>
        <div class="col-sm-10">
            {{ form.render('serviceDate') }}
        </div>
    </div>
    <br>
    <div>
        <div class="col-sm-10">
            {{ form.render('documentImage') }}
        </div>
    </div>
    <br>
    <div>
        <div class="col-sm-10">
            {{ form.render('guideImage') }}
        </div>
    </div>
    <br>
    <div>
        <div class="col-sm-10">
            {{ form.render('admissionImage') }}
        </div>
    </div>
    <br>
    <div>
        <div class="col-sm-10">
            {{ form.render('deliverImage') }}
        </div>
    </div>
    <br>
    <div>
        <div class="col-sm-10">
            {{ form.render('complaintType') }}
        </div>
    </div>
    <br>
    <div>
        <div class="col-sm-10">
            {{ form.render('dueDate') }}
        </div>
    </div>
    <br>
    <div>
        <div class="col-sm-10">
            {{ form.render('createdAt') }}
        </div>
    </div>
    <br>
    <div>
        <div class="col-sm-10">
            {{ form.render('updatedAt') }}
        </div>
    </div>
    {{ submit_button("Guardar", "class": "btn btn-success btn-sm pull-right") }}
</form>