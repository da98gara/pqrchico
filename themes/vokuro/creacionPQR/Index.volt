{%- set menus = [
    'Home': null,
    'Users': 'users',
    'Profiles': 'profiles',
    'Permissions': 'permissions'
] -%}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PQRCliente</title>
</head>
<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <img alt="image" class="rounded-circle" src="public/img/1.png" width="90"/>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="block m-t-xs font-bold">{{ auth.getName() }}</span>
                                <span class="text-muted text-xs block">Mas Opciones<b class="caret"></b></span>
                            </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li> {{ link_to('users/changePassword', 'class': 'dropdown-item', 'Cambiar Contraseña') }}</li>
                                <li>{{ link_to('session/logout', 'class': 'nav-link', 'Cerrar Sesion') }}</li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            PQR:.
                        </div>
                    </li>
                    <li class="active">
                        <a href="index.html"><i class="fa fa-th-large"></i> <span class="nav-label">Tu menu</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="dashboard_2.html">Crear:.PQR:.</a></li>
                            <li><a href="dashboard_2.html">Consultar Estado de PQR</a></li>
                        </ul>
                    </li>
                </ul>

            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
</body>
</html>