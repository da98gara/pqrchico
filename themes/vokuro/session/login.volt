<div class="text-center">
    {{ flash.output() }}
</div>
<h1 class="text-center">INICIAR SESIÓN</h1>
<div>
    <div class="loginColumns animated fadeInDown ">
        <hr>
        <div class="row">
            <div class="col-md-6">
                <h2 class="font-bold">Bienvenido a PQR:.CHICO:.</h2>

                <p>
                    Perfecta interacción sobre sistema de información para la verificación de
                    PQR.
                </p>

                <p>
                    Portabilidad en su plataforma para dar una pronta gestión a las PQR
                </p>

                <p>
                    Registro sobre la plataforma e interacción sobre los diferentes Módulos
                </p>

                <p>
                    <small>It has survived not only five centuries, but also the leap into electronic typesetting,
                        remaining essentially unchanged.</small>
                </p>
            </div>
     
            <div class="col-md-6">
                <div class="ibox-content">
                    <form method="post">
                        <div class="form-group">
                            {{ form.render('email', ['class': 'form-control', 'id': 'email-input', 'aria-describedby': 'emailHelp', 'placeholder': 'Correo']) }}
                            <small id="emailHelp" class="form-text text-muted">Nunca compartiremos tu correo electrónico
                                con nadie </small>
                        </div>
                        <div class="form-group">
                            {{ form.render('password', ['class': 'form-control', 'id': 'password-input', 'placeholder': 'Contraseña']) }}
                        </div>
                        <div class="form-group form-check">
                            {{ form.render('remember', ['class': 'form-check-input']) }}
                            {{ form.label('remember', ['class': 'form-check-label', 'for': 'login-remember']) }}
                        </div>

                        {{ form.render('csrf', ['value': security.getToken()]) }}

                        {{ form.render('Login') }} 

                        <a href="#">
                            <div class="text-center ">
                                <small>{{ link_to('session/forgotPassword', '¿ Olvidaste tu Contraseña?') }}</small>
                            </div>
                        </a>

                        <p class="btn btn-sm btn-white btn-block mt-2">{{ link_to('session/signup', 'Registrate') }}</p>
                    </form>

                </div>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-md-6">
                Copyright
            </div>
            <div class="col-md-6 text-right">
                <small>© 2014-2020</small>
            </div>
        </div>

    </div>
</div>