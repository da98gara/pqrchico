{% set isNameValidClass = form.messages('name') ? 'form-control is-invalid' : 'form-control' %}
{% set isEmailValidClass = form.messages('email') ? 'form-control is-invalid' : 'form-control' %}
{% set isPasswordValidClass = form.messages('password') ? 'form-control is-invalid' : 'form-control' %}
{% set isConfigPasswordValidClass = form.messages('confirmPassword') ? 'form-control is-invalid' : 'form-control' %}
{% set isETermsValidClass = form.messages('terms') ? 'form-check-input is-invalid' : 'form-check-input' %}
{{ flash.output() }}
<h1>Registro de Usuario</h1>
<hr>
<div>
    <div class="middle-box text-center loginscreen   animated fadeInDown">
        <div>
            <div class="mr-5">

                <h2>Registro PQR:.CHICO:.</h2>
                <p>Create una cuenta para ver la acción.</p>
            </div>
            <form role="form" method="post">
                <div class="form-group ">
                    <div class="col">
                        {{ form.render('name', ['class': isNameValidClass, 'placeholder': 'Nombre']) }}
                        <div class="invalid-feedback">
                            {{ form.messages('name') }}
                        </div>
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col">
                        {{ form.render('email', ['class': isEmailValidClass, 'placeholder': 'Correo']) }}
                        <div class="invalid-feedback">
                            {{ form.messages('email') }}
                        </div>
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col">
                        {{ form.render('password', ['class': isPasswordValidClass, 'placeholder': 'Contraseña']) }}
                        <div class="invalid-feedback">
                            {{ form.messages('password') }}
                        </div>
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col">
                        {{ form.render('confirmPassword', ['class': isConfigPasswordValidClass, 'placeholder': 'Confirmar Contraseña']) }}
                        <div class="invalid-feedback">
                            {{ form.messages('confirmPassword') }}
                        </div>
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col">
                        <div class="form-check">
                            {{ form.render('terms', ['class': isETermsValidClass]) }}
                            {{ form.label('terms', ['class': 'form-check-label']) }}
                        </div>
                        <div class="invalid-feedback">
                            {{ form.messages('terms') }}
                        </div>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col">
                        {{ form.render('csrf', ['value': security.getToken()]) }}
                        {{ form.messages('csrf') }}

                        {{ form.render('Sign Up') }}

                        <p class="text-muted text-center"><small>Ya tienes una cuenta?</small></p>

                        <div class="btn-sm btn-white btn-block">
                            <a href="#">

                                {{ link_to('session/login', "Iniciar Sesion") }}
                            </a>

                        </div>
                        <p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small> </p>
                    </div>

                </div>
            </form>

        </div>
    </div>
</div>
<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.js"></script>
<!-- iCheck -->
<script src="js/plugins/iCheck/icheck.min.js"></script>
<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    });
</script>


<hr>