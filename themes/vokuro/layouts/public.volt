{%- set menus = [
    'Home': 'index',
    'About': 'about'
] -%}

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    {{ link_to(null, 'class': 'navbar-brand', 'PQR:.CHICO:.') }}

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            {%- for key, value in menus %}
            {% if value == dispatcher.getControllerName() %}
            <li class="nav-item active">
                {{ link_to(value, 'class': 'nav-link', 'Inicio' ) }}
            </li>
            {% else %}
            <li class="nav-item">{{ link_to(value, 'class': 'nav-link','Acerca de') }}</li>
            {% endif %}
            {%- endfor -%}
        </ul>

        <ul class="navbar-nav my-2 my-lg-0">
            {%- if logged_in is defined and not(logged_in is empty) -%}
            <li class="nav-item">{{ link_to('users', 'class': 'nav-link', 'Panel de Usuarios') }}</li>
            <li class="nav-item">{{ link_to('session/logout', 'class': 'nav-link', 'Cerrar sesión') }}</li>
            {% else %}
            <li class="nav-item">{{ link_to('session/login', 'class': 'nav-link', 'Iniciar Sesión') }}</li>
            {% endif %}
        </ul>
    </div>
</nav>

<main role="main" class="flex-shrink-0">
    <div class="container">
        {{ content() }}
    </div>
</main>

<footer class="footer mt-auto py-3 bg-primary ">
    <div class="container">
        <span class="text-muted">
            <div class="text-center text-light" >

              {{ link_to("privacy", "Privacy Policy") }}
                {{ link_to("terms", "Terms") }}
    
                © {{ date("Y") }} Phalcon Team.
            </div>
            
        </span>
    </div>
</footer>