<?php
/**
 * @var \Phalcon\Mvc\View\Engine\Php $this
 */
?>

<div class="row">
    <nav>
        <ul class="pager">
            <li class="previous"><?php echo $this->tag->linkTo(["complaintparameters", "Back"]) ?></li>
        </ul>
    </nav>
</div>

<div class="page-header">
    <h1>Edit complaintparameters</h1>
</div>

<?php echo $this->getContent(); ?>

<form action="complaintparameters/save" class="form-horizontal" method="post">
    <div class="form-group">
    <label for="fieldPqremail" class="col-sm-2 control-label">PqrEmail</label>
    <div class="col-sm-10">
        <?php echo $this->tag->textField(["pqrEmail", "size" => 30, "class" => "form-control", "id" => "fieldPqremail"]) ?>
    </div>
</div>

<div class="form-group">
    <label for="fieldResponsemodel" class="col-sm-2 control-label">ResponseModel</label>
    <div class="col-sm-10">
        <?php echo $this->tag->textField(["responseModel", "size" => 30, "class" => "form-control", "id" => "fieldResponsemodel"]) ?>
    </div>
</div>

<div class="form-group">
    <label for="fieldDaysdue" class="col-sm-2 control-label">DaysDue</label>
    <div class="col-sm-10">
        <?php echo $this->tag->textField(["daysDue", "type" => "number", "class" => "form-control", "id" => "fieldDaysdue"]) ?>
    </div>
</div>

<div class="form-group">
    <label for="fieldNextnumber" class="col-sm-2 control-label">NextNumber</label>
    <div class="col-sm-10">
        <?php echo $this->tag->textField(["nextNumber", "type" => "number", "class" => "form-control", "id" => "fieldNextnumber"]) ?>
    </div>
</div>

<div class="form-group">
    <label for="fieldOperatorid" class="col-sm-2 control-label">OperatorId</label>
    <div class="col-sm-10">
        <?php echo $this->tag->textField(["operatorId", "size" => 30, "class" => "form-control", "id" => "fieldOperatorid"]) ?>
    </div>
</div>

<div class="form-group">
    <label for="fieldCreatedAt" class="col-sm-2 control-label">Created</label>
    <div class="col-sm-10">
        <?php echo $this->tag->textField(["created_at", "size" => 30, "class" => "form-control", "id" => "fieldCreatedAt"]) ?>
    </div>
</div>

<div class="form-group">
    <label for="fieldUpdatedAt" class="col-sm-2 control-label">Updated</label>
    <div class="col-sm-10">
        <?php echo $this->tag->textField(["updated_at", "size" => 30, "class" => "form-control", "id" => "fieldUpdatedAt"]) ?>
    </div>
</div>


    <?php echo $this->tag->hiddenField("id") ?>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?php echo $this->tag->submitButton(["Save", "class" => "btn btn-default"]) ?>
        </div>
    </div>
</form>
