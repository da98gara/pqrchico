<?php
/**
 * @var \Phalcon\Mvc\View\Engine\Php $this
 */
?>

<div class="row">
    <nav>
        <ul class="pager">
            <li class="previous"><?php echo $this->tag->linkTo(["complaintparameters/index", "Go Back"]); ?></li>
            <li class="next"><?php echo $this->tag->linkTo(["complaintparameters/new", "Create "]); ?></li>
        </ul>
    </nav>
</div>

<div class="page-header">
    <h1>Search result</h1>
</div>

<?php echo $this->getContent(); ?>

<div class="row">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Id</th>
            <th>PqrEmail</th>
            <th>ResponseModel</th>
            <th>DaysDue</th>
            <th>NextNumber</th>
            <th>OperatorId</th>
            <th>Created</th>
            <th>Updated</th>

                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($page->getItems() as $complaintparameter): ?>
            <tr>
                <td><?php echo $complaintparameter->getId() ?></td>
            <td><?php echo $complaintparameter->getPqremail() ?></td>
            <td><?php echo $complaintparameter->getResponsemodel() ?></td>
            <td><?php echo $complaintparameter->getDaysdue() ?></td>
            <td><?php echo $complaintparameter->getNextnumber() ?></td>
            <td><?php echo $complaintparameter->getOperatorid() ?></td>
            <td><?php echo $complaintparameter->getCreatedAt() ?></td>
            <td><?php echo $complaintparameter->getUpdatedAt() ?></td>

                <td><?php echo $this->tag->linkTo(["complaintparameters/edit/" . $complaintparameter['id'], "Edit"]); ?></td>
                <td><?php echo $this->tag->linkTo(["complaintparameters/delete/" . $complaintparameter['id'], "Delete"]); ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div class="row">
    <div class="col-sm-1">
        <p class="pagination" style="line-height: 1.42857;padding: 6px 12px;">
            <?php echo $page->getCurrent(), "/", $page->getTotalItems() ?>
        </p>
    </div>
    <div class="col-sm-11">
        <nav>
            <ul class="pagination">
                <li><?php echo $this->tag->linkTo(["complaintparameters/search", "First", 'class' => 'page-link', 'id' => 'first']) ?></li>
                <li><?php echo $this->tag->linkTo(["complaintparameters/search?page=" . $page->getPrevious(), "Previous", 'class' => 'page-link', 'id' => 'previous']) ?></li>
                <li><?php echo $this->tag->linkTo(["complaintparameters/search?page=" . $page->getNext(), "Next", 'class' => 'page-link', 'id' => 'next']) ?></li>
                <li><?php echo $this->tag->linkTo(["complaintparameters/search?page=" . $page->getLast(), "Last", 'class' => 'page-link', 'id' => 'last']) ?></li>
            </ul>
        </nav>
    </div>
</div>
