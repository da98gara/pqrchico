<div class="row">
    <nav>
        <ul class="pager">
            <li class="previous">{{ link_to("complaintsandclaims", "Go Back") }}</li>
        </ul>
    </nav>
</div>

<div class="page-header">
    <h1>Edit complaintsandclaims</h1>
</div>

{{ content() }}

<form action="complaintsandclaims/save" class="form-horizontal" method="post">
    <div class="form-group">
    <label for="fieldCun" class="col-sm-2 control-label">Cun</label>
    <div class="col-sm-10">
        {{ text_field("cun", "size" : 30, "class" : "form-control", "id" : "fieldCun") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldIdentification" class="col-sm-2 control-label">Identification</label>
    <div class="col-sm-10">
        {{ text_field("identification", "type" : "numeric", "class" : "form-control", "id" : "fieldIdentification") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldComplainttext" class="col-sm-2 control-label">ComplaintText</label>
    <div class="col-sm-10">
        {{ text_field("complaintText", "size" : 30, "class" : "form-control", "id" : "fieldComplainttext") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldComplaintdate" class="col-sm-2 control-label">ComplaintDate</label>
    <div class="col-sm-10">
        {{ text_field("complaintDate", "size" : 30, "class" : "form-control", "id" : "fieldComplaintdate") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldSendername" class="col-sm-2 control-label">SenderName</label>
    <div class="col-sm-10">
        {{ text_field("senderName", "size" : 30, "class" : "form-control", "id" : "fieldSendername") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldSenderaddress" class="col-sm-2 control-label">SenderAddress</label>
    <div class="col-sm-10">
        {{ text_field("senderAddress", "size" : 30, "class" : "form-control", "id" : "fieldSenderaddress") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldRecipientname" class="col-sm-2 control-label">RecipientName</label>
    <div class="col-sm-10">
        {{ text_field("recipientName", "size" : 30, "class" : "form-control", "id" : "fieldRecipientname") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldRecipientaddress" class="col-sm-2 control-label">RecipientAddress</label>
    <div class="col-sm-10">
        {{ text_field("recipientAddress", "size" : 30, "class" : "form-control", "id" : "fieldRecipientaddress") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldServicedate" class="col-sm-2 control-label">ServiceDate</label>
    <div class="col-sm-10">
        {{ text_field("serviceDate", "type" : "date", "class" : "form-control", "id" : "fieldServicedate") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldDocumentimage" class="col-sm-2 control-label">DocumentImage</label>
    <div class="col-sm-10">
        {{ text_field("documentImage", "size" : 30, "class" : "form-control", "id" : "fieldDocumentimage") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldGuideimage" class="col-sm-2 control-label">GuideImage</label>
    <div class="col-sm-10">
        {{ text_field("guideImage", "size" : 30, "class" : "form-control", "id" : "fieldGuideimage") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldAdmissionimage" class="col-sm-2 control-label">AdmissionImage</label>
    <div class="col-sm-10">
        {{ text_field("admissionImage", "size" : 30, "class" : "form-control", "id" : "fieldAdmissionimage") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldDeliverimage" class="col-sm-2 control-label">DeliverImage</label>
    <div class="col-sm-10">
        {{ text_field("deliverImage", "size" : 30, "class" : "form-control", "id" : "fieldDeliverimage") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldComplainttype" class="col-sm-2 control-label">ComplaintType</label>
    <div class="col-sm-10">
        {{ text_field("complaintType", "size" : 30, "class" : "form-control", "id" : "fieldComplainttype") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldDuedate" class="col-sm-2 control-label">DueDate</label>
    <div class="col-sm-10">
        {{ text_field("dueDate", "size" : 30, "class" : "form-control", "id" : "fieldDuedate") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldCreatedAt" class="col-sm-2 control-label">Created</label>
    <div class="col-sm-10">
        {{ text_field("created_at", "size" : 30, "class" : "form-control", "id" : "fieldCreatedAt") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldUpdatedAt" class="col-sm-2 control-label">Updated</label>
    <div class="col-sm-10">
        {{ text_field("updated_at", "size" : 30, "class" : "form-control", "id" : "fieldUpdatedAt") }}
    </div>
</div>


    {{ hidden_field("id") }}

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            {{ submit_button('Send', 'class': 'btn btn-default') }}
        </div>
    </div>
</form>
