<div class="row">
    <nav>
        <ul class="pager">
            <li class="previous">{{ link_to("complaintsandclaims/index", "Go Back") }}</li>
            <li class="next">{{ link_to("complaintsandclaims/new", "Create ") }}</li>
        </ul>
    </nav>
</div>

<div class="page-header">
    <h1>Search result</h1>
</div>

{{ content() }}

<div class="row">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Id</th>
            <th>Cun</th>
            <th>Identification</th>
            <th>ComplaintText</th>
            <th>ComplaintDate</th>
            <th>SenderName</th>
            <th>SenderAddress</th>
            <th>RecipientName</th>
            <th>RecipientAddress</th>
            <th>ServiceDate</th>
            <th>DocumentImage</th>
            <th>GuideImage</th>
            <th>AdmissionImage</th>
            <th>DeliverImage</th>
            <th>ComplaintType</th>
            <th>DueDate</th>
            <th>Created</th>
            <th>Updated</th>

                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        {% for complaintsandclaim in page.getItems() %}
            <tr>
                <td>{{ complaintsandclaim['id'] }}</td>
            <td>{{ complaintsandclaim['cun'] }}</td>
            <td>{{ complaintsandclaim['identification'] }}</td>
            <td>{{ complaintsandclaim['complaintText'] }}</td>
            <td>{{ complaintsandclaim['complaintDate'] }}</td>
            <td>{{ complaintsandclaim['senderName'] }}</td>
            <td>{{ complaintsandclaim['senderAddress'] }}</td>
            <td>{{ complaintsandclaim['recipientName'] }}</td>
            <td>{{ complaintsandclaim['recipientAddress'] }}</td>
            <td>{{ complaintsandclaim['serviceDate'] }}</td>
            <td>{{ complaintsandclaim['documentImage'] }}</td>
            <td>{{ complaintsandclaim['guideImage'] }}</td>
            <td>{{ complaintsandclaim['admissionImage'] }}</td>
            <td>{{ complaintsandclaim['deliverImage'] }}</td>
            <td>{{ complaintsandclaim['complaintType'] }}</td>
            <td>{{ complaintsandclaim['dueDate'] }}</td>
            <td>{{ complaintsandclaim['created_at'] }}</td>
            <td>{{ complaintsandclaim['updated_at'] }}</td>

                <td>{{ link_to("complaintsandclaims/edit/"~complaintsandclaim['id'], "Edit") }}</td>
                <td>{{ link_to("complaintsandclaims/delete/"~complaintsandclaim['id'], "Delete") }}</td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
</div>

<div class="row">
    <div class="col-sm-1">
        <p class="pagination" style="line-height: 1.42857;padding: 6px 12px;">
            {{ page.getCurrent()~"/"~page.getTotalItems() }}
        </p>
    </div>
    <div class="col-sm-11">
        <nav>
            <ul class="pagination">
                <li>{{ link_to("complaintsandclaims/search", "First", false, "class": "page-link", 'id': 'first') }}</li>
                <li>{{ link_to("complaintsandclaims/search?page="~page.getPrevious(), "Previous", false, "class": "page-link", 'id': 'previous') }}</li>
                <li>{{ link_to("complaintsandclaims/search?page="~page.getNext(), "Next", false, "class": "page-link", 'id': 'next') }}</li>
                <li>{{ link_to("complaintsandclaims/search?page="~page.getLast(), "Last", false, "class": "page-link", 'id': 'last') }}</li>
            </ul>
        </nav>
    </div>
</div>
