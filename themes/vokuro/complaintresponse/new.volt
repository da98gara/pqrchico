<div class="row">
    <nav>
        <ul class="pager">
            <li class="previous">{{ link_to("complaintresponse", "Go Back") }}</li>
        </ul>
    </nav>
</div>

<div class="page-header">
    <h1>Create complaintresponse</h1>
</div>

{{ content() }}

<form action="complaintresponse/create" class="form-horizontal" method="post">
    <div class="form-group">
    <label for="fieldComplaintId" class="col-sm-2 control-label">Complaint</label>
    <div class="col-sm-10">
        {{ text_field("complaint_id", "type" : "numeric", "class" : "form-control", "id" : "fieldComplaintId") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldResponsedate" class="col-sm-2 control-label">ResponseDate</label>
    <div class="col-sm-10">
        {{ text_field("responseDate", "size" : 30, "class" : "form-control", "id" : "fieldResponsedate") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldResponse" class="col-sm-2 control-label">Response</label>
    <div class="col-sm-10">
        {{ text_field("response", "size" : 30, "class" : "form-control", "id" : "fieldResponse") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldResponseuser" class="col-sm-2 control-label">ResponseUser</label>
    <div class="col-sm-10">
        {{ text_field("responseUser", "size" : 30, "class" : "form-control", "id" : "fieldResponseuser") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldCreatedAt" class="col-sm-2 control-label">Created</label>
    <div class="col-sm-10">
        {{ text_field("created_at", "size" : 30, "class" : "form-control", "id" : "fieldCreatedAt") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldUpdatedAt" class="col-sm-2 control-label">Updated</label>
    <div class="col-sm-10">
        {{ text_field("updated_at", "size" : 30, "class" : "form-control", "id" : "fieldUpdatedAt") }}
    </div>
</div>


    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            {{ submit_button('Save', 'class': 'btn btn-default') }}
        </div>
    </div>
</form>
