<div class="row">
    <nav>
        <ul class="pager">
            <li class="previous">{{ link_to("complaintresponse/index", "Go Back") }}</li>
            <li class="next">{{ link_to("complaintresponse/new", "Create ") }}</li>
        </ul>
    </nav>
</div>

<div class="page-header">
    <h1>Search result</h1>
</div>

{{ content() }}

<div class="row">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Id</th>
            <th>Complaint</th>
            <th>ResponseDate</th>
            <th>Response</th>
            <th>ResponseUser</th>
            <th>Created</th>
            <th>Updated</th>

                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        {% for complaintresponse in page.getItems() %}
            <tr>
                <td>{{ complaintresponse['id'] }}</td>
            <td>{{ complaintresponse['complaint_id'] }}</td>
            <td>{{ complaintresponse['responseDate'] }}</td>
            <td>{{ complaintresponse['response'] }}</td>
            <td>{{ complaintresponse['responseUser'] }}</td>
            <td>{{ complaintresponse['created_at'] }}</td>
            <td>{{ complaintresponse['updated_at'] }}</td>

                <td>{{ link_to("complaintresponse/edit/"~complaintresponse['id'], "Edit") }}</td>
                <td>{{ link_to("complaintresponse/delete/"~complaintresponse['id'], "Delete") }}</td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
</div>

<div class="row">
    <div class="col-sm-1">
        <p class="pagination" style="line-height: 1.42857;padding: 6px 12px;">
            {{ page.getCurrent()~"/"~page.getTotalItems() }}
        </p>
    </div>
    <div class="col-sm-11">
        <nav>
            <ul class="pagination">
                <li>{{ link_to("complaintresponse/search", "First", false, "class": "page-link", 'id': 'first') }}</li>
                <li>{{ link_to("complaintresponse/search?page="~page.getPrevious(), "Previous", false, "class": "page-link", 'id': 'previous') }}</li>
                <li>{{ link_to("complaintresponse/search?page="~page.getNext(), "Next", false, "class": "page-link", 'id': 'next') }}</li>
                <li>{{ link_to("complaintresponse/search?page="~page.getLast(), "Last", false, "class": "page-link", 'id': 'last') }}</li>
            </ul>
        </nav>
    </div>
</div>
